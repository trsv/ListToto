﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using ListToto.data;

namespace ListToto.io
{
    // ио Тиража
    class CirculationIO
    {

        private const string @DEFAULT_NAME_XML = "out.xml";
        private const string @PATH_TO_SAVE = "out/";

        static CirculationIO()
        {
            System.IO.Directory.CreateDirectory(PATH_TO_SAVE);
        }

        public static void write(List<Circulation> list, string @path = DEFAULT_NAME_XML)
        {
            foreach (Circulation circulation in list)
            {
                write(circulation, path);
            }

        }


        public static void write(Circulation circulation, string @path = DEFAULT_NAME_XML)
        {

            path = PATH_TO_SAVE + path;

            var settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Indent = true;
            settings.NewLineOnAttributes = true;

            XmlWriter writer = XmlWriter.Create(path, settings);

            writer.WriteStartDocument();
            writer.WriteStartElement("Data");

            writer.WriteStartElement("SelectedPos");
            foreach (Match match in circulation.listMatch) writer.WriteElementString("Item", match.exodus.ToString());
            for (int other = circulation.listMatch.Count; other < 20; other++) writer.WriteElementString("Item", "0");
            writer.WriteEndElement();

            writer.WriteElementString("Count", circulation.listMatch.Count.ToString());

            writer.WriteStartElement("List1");
            foreach (Match match in circulation.listMatch) writer.WriteElementString("Item", match.probabilityOfPlayers1.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("ListX");
            foreach (Match match in circulation.listMatch) writer.WriteElementString("Item", match.probabilityOfPlayersX.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("List2");
            foreach (Match match in circulation.listMatch) writer.WriteElementString("Item", match.probabilityOfPlayers2.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Num1");
            foreach (KeyValuePair<int, string> entry in circulation.listPrizeFund) writer.WriteElementString("Item", entry.Value);
            writer.WriteEndElement();

            writer.WriteStartElement("Num2");
            foreach (KeyValuePair<int, string> entry in circulation.listCatch) writer.WriteElementString("Item", entry.Value);
            writer.WriteEndElement();

            writer.WriteElementString("From", "0");
            writer.WriteElementString("To", circulation.listMatch.Count.ToString());

            writer.WriteEndElement(); // end Data
            writer.WriteEndDocument(); // end document
            writer.Close();
        }


    }
}
