﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ListToto.net;

namespace ListToto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        async private void buttonGo_Click(object sender, EventArgs e)
        {
            try
            {
                DonorHandler handler = new DonorHandler();

                var progress = new Progress<DonorHandler>();
                progress.ProgressChanged += (o, task) =>
                {
                    progressBar.Value = task.progressPercent;
                    progressBar.Update();

                    progressLabel.Text = task.progressMessage;
                };

                buttonGo.Enabled = false;
                progressBar.Visible = true;
                progressLabel.Visible = true;

                await handler.process(progress);

                buttonGo.Enabled = true;
            }
            catch (Exception ex)
            {
                buttonGo.Enabled = true;
                MessageBox.Show(ex.Message, "Ошибка");
            }
        }
    }
}
