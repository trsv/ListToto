﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace ListToto.data
{
    // класс Тираж
    class Circulation
    {
        // список матчей в тираже
        public List<Match> listMatch { get; set; } = new List<Match>(20);

        // призовой фонд
        public SortedDictionary<int, string> listPrizeFund { get; set; } = new SortedDictionary<int, string>();

        // угадано (грн)
        public SortedDictionary<int, string> listCatch { get; set; } = new SortedDictionary<int, string>();

        // название тиража
        public string name { get; set; }

        // нужны доп. рассчеты
        public bool needProcess { get; set; }


        // вычисляем список призового фонда
        public void processListPrizeFund(int fund, int jackpot)
        {
            listPrizeFund.Add(15, calculateValue(fund, 0.1, jackpot));
            listPrizeFund.Add(14, calculateValue(fund, 0.1));
            listPrizeFund.Add(13, calculateValue(fund, 0.1));
            listPrizeFund.Add(12, calculateValue(fund, 0.1));
            listPrizeFund.Add(11, calculateValue(fund, 0.1));

            listPrizeFund.Add(10, calculateValue(fund, 0.18));
            listPrizeFund.Add(9, calculateValue(fund, 0.32));
        }

        // вычисляем список угадано
        public void processListCatch(int jackpot)
        {
            double average = calculateAverage();

            Dictionary<int, double> listCoeff = new Dictionary<int, double>();
            listCoeff.Add(9, calculateCoeff9(average));
            listCoeff.Add(10, listCoeff[9] * 2);
            listCoeff.Add(11, listCoeff[10] * 3);
            listCoeff.Add(12, listCoeff[11] * 5);
            listCoeff.Add(13, listCoeff[12] * 10);
            listCoeff.Add(14, listCoeff[13] * 5);
            listCoeff.Add(15, jackpot / 20);

            for (int category = 9; category <= 15; category++)
            {
                double value = Convert.ToDouble(listPrizeFund[category]) / listCoeff[category];
                listCatch.Add(category, ((int)value).ToString());
            }
        }

        // вычислить значение призового фонда для категории
        private string calculateValue(double fund, double percent, double jackpot = 0)
        {
            double value = fund * percent + jackpot;
            return ((int) value).ToString();
        }

        // вычисляем среднее значение исходов
        private double calculateAverage()
        {
            double sum = 0.0;
            foreach (Match match in listMatch) sum += match.probabilityForCalculate;
            return sum / listMatch.Count;
        }

        // вычисляем коэфф. для 9й группы
        private double calculateCoeff9(double average)
        {
            if (average >= 40.0) return 1.0;
            else if (average < 40.0 && average >= 39.0) return 2.0;
            else if (average < 39.0 && average >= 38.0) return 3.0;
            else if (average < 38.0 && average >= 37.0) return 4.0;
            else if (average < 37.0 && average >= 36.0) return 5.0;
            else if (average < 36.0 && average >= 35.0) return 6.0;
            else if (average < 35.0 && average >= 34.0) return 7.0;
            else if (average < 34.0 && average >= 33.0) return 9.0;
            else if (average < 33.0 && average >= 32.0) return 11.0;
            else if (average < 32.0 && average >= 31.0) return 13.0;
            else if (average < 31.0 && average >= 30.0) return 15.0;
            else if (average < 30.0 && average >= 29.0) return 17.0;
            else if (average < 29.0 && average >= 28.0) return 19.0;
            else if (average < 28.0 && average >= 27.0) return 27.0;
            else return 23.0;
        }
    }
}
