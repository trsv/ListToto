﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListToto.data
{
    // класс Матча
    class Match
    {
        // исход матча
        public string score { get; set; }
        public int exodus { get; set; }

        // вероятность игроков
        public string probabilityOfPlayers1 { get; set; }
        public string probabilityOfPlayersX { get; set; }
        public string probabilityOfPlayers2 { get; set; }

        // вероятность для ручных вычислений
        public double probabilityForCalculate { get; set; }


        public void processProbabilityForCalculate()
        {
            switch (exodus)
            {
                case 4: probabilityForCalculate = Convert.ToDouble(probabilityOfPlayers1.Replace(".", ",")); break;
                case 2: probabilityForCalculate = Convert.ToDouble(probabilityOfPlayersX.Replace(".", ",")); break;
                case 1: probabilityForCalculate = Convert.ToDouble(probabilityOfPlayers2.Replace(".", ",")); break;
                default: probabilityForCalculate = 33.33; break;
            }
        }

        public void processExodus()
        {
            exodus = x12(score);
        }

        // вычисление исхода (1 x 2)
        private int x12(int score1, int score2)
        {
            if (score1 > score2) return 4;
            else if (score1 < score2) return 1;
            else return 2;
        }

        // вычисление исхода (1 x 2) по строке
        private int x12(string score)
        {
            if (score.ToLower().Equals("н/с")) return 7;

            string[] scores = score.Split(':');
            int score1 = Convert.ToInt32(scores[0]);
            int score2 = Convert.ToInt32(scores[1]);

            return x12(score1, score2);
        }
    }
}
