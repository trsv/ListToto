﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

using ListToto.data;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace ListToto.net
{
    // класс паринга xbetua
    class Donor
    {

        private const string URL = @"https://1xbetua.com/toto/fifteen/list";
        private const string HOST = "1xbetua.com";
        private const string USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
        private const string ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
        private const string LANGUAGE = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7";

        public string urlNextPage { get; set; }
        public bool hasNext { get; set; } = false;

        private HtmlDocument doc { get; set; } = new HtmlDocument();
        private CookieContainer cookies = new CookieContainer();




        // авторизация
        public async Task<Boolean> auth()
        {
            Console.WriteLine("Auth process...");

            bool success = false;

            using (var handler = new HttpClientHandler() { CookieContainer = cookies })
            using (HttpClient client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Host", HOST);
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Accept", "application/json, text/javascript, */*; q=0.01");
                client.DefaultRequestHeaders.Add("Origin", "https://1xbetua.com");
                client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                //client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                client.DefaultRequestHeaders.Add("Referer", "https://1xbetua.com/");
                client.DefaultRequestHeaders.Add("Accept-Language", LANGUAGE);

                var args = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("uLogin", "28657227"),
                    new KeyValuePair<string, string>("uPassword", "f57e2r4t")
                });

                using (var response = await client.PostAsync("https://1xbetua.com/user/auth/", args))
                {
                    using (var content = response.Content)
                    {
                        string json = await content.ReadAsStringAsync();
                        success = json.Contains("true");
                    }
                }

            }

            return success;
        }

        // парсим список ссылок на тиражи
        public async Task<List<string>> listUrl(string @url = URL)
        {
            List<string> list = new List<string>();

            using (var handler = new HttpClientHandler() { CookieContainer = cookies })
            using (HttpClient client = new HttpClient(handler))
            {
                Console.WriteLine("Go to: {0}", url.ToString());

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Host", HOST);
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                client.DefaultRequestHeaders.Add("Accept", ACCEPT);
                client.DefaultRequestHeaders.Add("Accept-Language", LANGUAGE);

                using (var response = await client.GetAsync(url))
                {
                    using (var content = response.Content)
                    {
                        string html = await content.ReadAsStringAsync();

                        doc.LoadHtml(html);

                        if (doc.DocumentNode == null) return null;

                        hasNext = setHasNextUrl();

                        HtmlNode table = doc.DocumentNode.SelectSingleNode("//table[contains(@class, 'tirazh__table')]");

                        foreach (HtmlNode row in table.SelectNodes(".//tr[not(@*)]"))
                        {
                            string status = row.SelectNodes(".//td[@class='text_left']").Last().InnerText;
                            if (status.Contains("Рассчитан"))
                            {
                                string urlCirculation = "https://1xbetua.com/" + row.SelectSingleNode(".//td/a").Attributes["href"].Value;
                                list.Add(urlCirculation);
                            }
                        }
                    }
                }

            }

            return list;
        }

        // запускает процесс парсинга тиража
        public async Task<Circulation> parse(string url)
        {
            Circulation circulation = await parseCirculation(url);
            return circulation;
        }

        // получаем страницу тиража
        private async Task<Circulation> parseCirculation(string url)
        {
            Circulation circulation = new Circulation();

            using (var handler = new HttpClientHandler() { CookieContainer = cookies })
            using (HttpClient client = new HttpClient(handler))
            {
                Console.WriteLine("Circulation page: {0}", url);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Host", HOST);
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                client.DefaultRequestHeaders.Add("User-Agent", USER_AGENT);
                client.DefaultRequestHeaders.Add("Accept", ACCEPT);
                client.DefaultRequestHeaders.Add("Referer", URL);
                client.DefaultRequestHeaders.Add("Accept-Language", LANGUAGE);

                using (var response = await client.GetAsync(url))
                {
                    using (var content = response.Content)
                    {
                        string html = await content.ReadAsStringAsync();

                        doc.LoadHtml(html);

                        if (doc.DocumentNode == null) return null;

                        circulation.needProcess = !html.Contains("Призовой фонд (UAH)");
                        parseCirculation(ref circulation);
                    }
                }

            }

            return circulation;
        }

        // парсим тираж
        private void parseCirculation(ref Circulation circulation)
        {
            // получаем матчи
            parseAllMatch(ref circulation);

            if (circulation == null) return;

            setFileName(ref circulation);

            if (circulation.needProcess) parseOtherDataWithProcess(ref circulation);
            else parseOtherData(ref circulation);

            // дополняем до 16 шт в списке
            for (int notAdded = 0; notAdded < 15; notAdded++)
            {
                if (!circulation.listCatch.ContainsKey(notAdded)) circulation.listCatch.Add(notAdded, "0");
                if (!circulation.listPrizeFund.ContainsKey(notAdded)) circulation.listPrizeFund.Add(notAdded, "0");
            }

        }

        // парсим список матчей
        private void parseAllMatch(ref Circulation circulation)
        {
            try
            {
                HtmlNode table = doc.DocumentNode.SelectSingleNode("//table[@class='tirazh__table']");

                foreach (HtmlNode nodeMatch in table.SelectNodes(".//tr[not(@*)]"))
                {
                    HtmlNodeCollection values = nodeMatch.SelectNodes(".//td[not(@*)]");

                    data.Match match = new data.Match
                    {
                        score = values.ElementAt(1).InnerText,
                        probabilityOfPlayers1 = values.ElementAt(2).InnerText.Split('/').ElementAt(1).Trim(),
                        probabilityOfPlayersX = values.ElementAt(3).InnerText.Split('/').ElementAt(1).Trim(),
                        probabilityOfPlayers2 = values.ElementAt(4).InnerText.Split('/').ElementAt(1).Trim()
                    };

                    match.processExodus();
                    match.processProbabilityForCalculate();

                    //Console.WriteLine("Score: {0}", match.score);
                    //Console.WriteLine("Probability: \t 1: {0} \t X: {1} \t 2: {2}",
                    //    match.probabilityOfPlayers1,
                    //    match.probabilityOfPlayersX,
                    //    match.probabilityOfPlayers2
                    //);
                    //Console.WriteLine("Exodus: {0}", match.exodus);

                    circulation.listMatch.Add(match);
                    //Console.WriteLine("---------");
                }
            }
            catch (Exception)
            {
                circulation = null;
            }
        }

        // парсим фонд, угадано
        private void parseOtherData(ref Circulation circulation)
        {
            HtmlNode table = doc.DocumentNode.SelectNodes("//table[@class='tirazh__table']").Last();

            foreach (HtmlNode nodeRow in table.SelectNodes(".//tr[not(@*)]"))
            {
                HtmlNodeCollection values = nodeRow.SelectNodes(".//td[not(@*)]");

                int nn = Convert.ToInt32(values.ElementAt(0).InnerText.Trim());

                // призовой фонд
                string fund = values.ElementAt(2).InnerText.Trim().Replace(" ", "");
                // угадано
                string catchCount = values.ElementAt(4).InnerText.Trim().Replace(" ", "");

                circulation.listPrizeFund.Add(nn, fund);
                circulation.listCatch.Add(nn, catchCount);
            }

            //circulation.listPrizeFund.ForEach(value => Console.Write("{0}\t", value));
            //Console.WriteLine("\n");
            //circulation.listCatch.ForEach(value => Console.Write("{0}\t", value));
            //Console.WriteLine("\n");
        }

        private void parseOtherDataWithProcess(ref Circulation circulation)
        {
            HtmlNode table = doc.DocumentNode.SelectNodes("//table[contains(@class, 'parameters__item-table')]").Last();

            string stringJackpot = table.SelectNodes(".//tr")[0].InnerText;
            string stringFund = table.SelectNodes(".//tr")[2].InnerText;

            Regex regex = new Regex(@"[^\d]");
            stringJackpot = regex.Replace(stringJackpot, "");
            stringFund = regex.Replace(stringFund, "");

            int jackpot = Convert.ToInt32(stringJackpot);
            int fund = Convert.ToInt32(stringFund);

            circulation.processListPrizeFund(fund, jackpot);
            circulation.processListCatch(jackpot);
        }

        private void setFileName(ref Circulation circulation)
        {
            circulation.name = doc.DocumentNode.SelectSingleNode("//h1").InnerText;
            circulation.name = System.Text.RegularExpressions.Regex.Match(circulation.name, @"\d+").Value;
            circulation.name += ".xml";
        }

        // получаем ссылку на след. страницу с тиражами
        private bool setHasNextUrl()
        {
            try
            {
                urlNextPage = "https://1xbetua.com/" + doc.DocumentNode.SelectNodes("//ul[@class='pagination']/li/a").Last().Attributes["href"].Value;
                Console.WriteLine("Next url: {0}", urlNextPage);
                return !string.IsNullOrWhiteSpace(urlNextPage);
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}