﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ListToto.data;
using ListToto.io;

namespace ListToto.net
{
    // хендлер парсеров
    class DonorHandler
    {
        public int progressPercent { get; set; } = 0;
        public string progressMessage { get; set; }


        public Task process(IProgress<DonorHandler> progress)
        {
            return Task.Run(() =>
            {
                // список тиражей
                List<Circulation> list = new List<Circulation>();

                /** xbetua start **/
                progressMessage = "Парсим: 1xbetua.com";

                Donor xbetua = new Donor();
                bool auth = xbetua.auth().Result;

                if (!auth) throw new Exception("Ошибка авторизации");

                int page = 0;

                do
                {
                    progressMessage = "Парсим: 1xbetua.com, страница: " + ++page;

                    progress.Report(this);

                    int circCount = 0;

                    // списпок ссылок на тиражи
                    List<string> listUrl;
                    if (page > 1) listUrl = xbetua.listUrl(xbetua.urlNextPage).Result;
                    else listUrl = xbetua.listUrl().Result;

                    //listUrl = new List<string>(); listUrl.Add("https://1xbetua.com/toto/fifteen/info/?id=877&type=1");

                    // парсим асинхронно тиражи по ссылкам
                    listUrl.ForEach(url =>
                    {
                        try
                        {
                            Circulation circulation = xbetua.parse(url).Result;
                            if (circulation != null) CirculationIO.write(circulation, circulation.name);

                            Console.WriteLine("[SAVED] {0} : calc {1}", circulation.name, circulation.needProcess);
                        }
                        catch (Exception e)
                        {
                            System.IO.File.AppendAllText("log.txt", url + "\n" + e.Message + "\n" + e.StackTrace + "\n----\n");
                        }
                        

                        progressPercent = ++circCount * 100 / listUrl.Count;
                        progress.Report(this);
                    });

                } while (xbetua.hasNext);

                /** xbetua end **/

                // выгрузка всего списка тиражей
                CirculationIO.write(list);

                progress.Report(this);

                this.progressPercent = 100;
                this.progressMessage = "Готово";

                progress.Report(this);

            });
        }

    }
}
